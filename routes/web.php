<?php
use App\Purchase;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'prevent-back-history'],function(){
    Auth::routes();
    Route::get('/','MainController@index');
    Route::post('/postlogin','MainController@postlogin');
    Route::get('/dashboard','MainController@homePage');
    Route::get('/logout','MainController@logout');
    Route::get('/ola','HomeController@index');

    /*---------------------------------------------------------------*/
    Route::resource('stock','Stock\StockController');


    Route::resource('product','Product\ProductController');
    /*----------------------------------------------------------------------------------*/
    Route::resource('purchase','Purchase\PurchaseController');

    /*----------------------------------------------------------------------------------*/

    /*---------------------------------------------------------------*/

    /*---------------------------------------------------------------*/
    Route::resource('account','Account\AccountController');
    /*---------------------------------------------------------------*/
    Route::resource('category','Category\CategoryController');
    /*---------------------------------------------------------------*/
    Route::resource('brand','Brand\BrandController');
    /*---------------------------------------------------------------*/
    Route::resource('list','listController');
    /*----------------------------------------------------------------*/
    Route::get('/sale/create','Sale\SaleController@create')->name('create');
    Route::get('/sale/partialcreate','Sale\SaleController@partialcreate')->name('partialcreate');
    Route::post('/sale/addStore','Sale\SaleController@addStore')->name('addStore');
    Route::get('/sale/list','Sale\SaleController@list')->name('sale_list');
    Route::get('/getproducts','Sale\SaleController@getproducts')->name('getproducts');
    /*-------------------------------------------------------------------------------------------------------------------------*/
    Route::get('/setting/change_password','Setting\SettingController@index')->name('change_password');
    Route::post('setting/change_password_action','Setting\SettingController@change_password_action')->name('change_password_action');
    Route::get('/setting/create_profile','Setting\SettingController@create_profile')->name('create_profile');
    /*--------------------------------------------------------------------------------------------------------*/

    Route::get('/report/purchase_report','Report\ReportController@index')->name('purchase_report');
    Route::get('report','Report\ReportController@report')->name('report');
    Route::get('/report/purchase_report/pdf','Report\ReportController@pdf');
    Route::get('/report/sale_report','Report\SaleController@index');
    Route::get('/sale_report','Report\SaleController@sale_report')->name('sale_report');
    Route::get('report/sale_report/pdf','Report\SaleController@pdf')->name('pdf');
    /*---------------------------------------------------------------------------------------------------------*/





    Route::get('/downloadPDF/{id}','listController@downloadPDF');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/test','listController@list')->name('list');


});


