
<table class="table" id="table">
    <thead>
    <tr>
        <th class="text-center" scope="col">S.N</th>
        <th class="text-center" scope="col">Product Name</th>
        <th class="text-center" scope="col">Batch</th>
        <th class="text-center" scope="col">Quantity</th>
        <th class="text-center" scope="col">Investment</th>
    </tr>
    </thead>
    <tbody>
    @foreach($purchases as $purchase)
        <tr>
            <th class="text-center" scope="row">{{$loop->iteration}}</th>
            <td class="text-center" >{{\App\Product::find($purchase->product_id)->name}}</td>
            <td class="text-center">{{$purchase->batch}}</td>
            <td class="text-center">{{$purchase->quantity}}</td>
            <td class="text-center">{{$purchase->investment}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td class="text-center"><strong>Total Amount:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-center" style="color: red">{{$purchases->sum('investment')}}</td>
    </tr>
    </tfoot>
</table>

