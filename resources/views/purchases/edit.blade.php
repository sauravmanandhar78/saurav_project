@extends('layouts.master')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif

   <div class="card">
       <div class="card-header border-2 border-top border-top-primary border-primary">
           <span style="font-size: 20px;">Purchase</span>
       </div>

       <div class="card-body">
           <form action="{{route('purchase.update',$purchases->id)}}" method="post" enctype="multipart/form-data">
               @method('PATCH')
               @csrf
               <div class="form-group">
                   <label for="product_name">Product</label>
                   <select name="product_id" id="product_id" class="form-control">
                       <option>Select Product</option>
                       @foreach($products as $product)
                           <option value="{{$product->id}}" {{$product->id == $purchases->product_id ? 'selected':''}}>{{$product->name}}</option>
                       @endforeach
                   </select>
               </div>
               <div class="form-group">
                   <label for="quantity">Quantity</label>
                   <input type="text" name="quantity" class="form-control" value="{{$purchases->quantity}}">
                   <a href="" style="color: red;">{{$errors->first('quantity')}}</a>
               </div>
               <div class="form-group">
                   <label for=buying_rate">Rate</label>
                   <input type="text" name="rate" class="form-control" value="{{$purchases->rate}}">
                   <a href="" style="color: red;">{{$errors->first('rate')}}</a>
               </div>
               <div class="form-group">
                   <label for=investment">Investment</label>
                   <input type="text" name="investment" class="form-control" value="{{$purchases->investment}}">
                   <a href="" style="color: red;">{{$errors->first('investment')}}</a>
               </div>
               <div class="form-group">
                   <button class="btn btn-primary">Update</button>
               </div>
           </form>
      </div>
   </div>
    {{--<div class="col-md-4 col-md-offset-4 bord">

    </div>--}}

    @endsection