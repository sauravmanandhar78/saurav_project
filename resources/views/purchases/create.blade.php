@extends('layouts.master')
@section('content')
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
            });
        });
    </script>
    @if(count($errors)>0)
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" id="message">
                {{$error}}
            </div>
        @endforeach
    @endif

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('purchase.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="product">Product</label>
                            <select name="product_id" id="product_id" class="form-control">
                                <option>Select Product:</option>
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>
                       {{-- <div class="form-group">
                            <label for="batch">Batch</label>
                            <input type="text" name="batch" class="form-control">
                        </div>--}}
                        <div class="form-group">
                            <label for="quantity">Quantity</label>
                            <input type="number" min="1" name="quantity" class="form-control">
                            <a href="" style="color: red;">{{$errors->first('quantity')}}</a>
                        </div>
                        <select name="select1">
                            <option value="" >Choose method...</option>
                            <option value="rate" id="rate">Rate</option>
                            <option value="investment"  id="investment">Investment</option>
                        </select>
                        <div class="investment">
                            <p>Investment
                                <input type="number"  name="investment" min="0" class="form-control" placeholder="Investment">
                                <a href="" style="color: red;">{{$errors->first('investment')}}</a>
                            </p>
                        </div>
                        <div class="rate">
                            <p>Rate
                                <input type="number" name="rate" id="rate" min="0" class="form-control" placeholder="Rate">
                                <a href="" style="color: red;">{{$errors->first('rate')}}</a>
                            </p>
                        </div>
                        <div class="form-group">
                            <br>
                            <button class="btn btn-primary col-md-12">Purchase</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
   <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Purchase</span>
            @if(Gate::check('isAdmin') || Gate::check('isManager'))
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float: right">
                <i class="fas fa-plus"></i>&nbsp;&nbsp;Purchase
            </button>
                @endif
        </div>
       <div class="card-body">
            <table class="table table-bordered" id="table" width="100%">
                <thead>
                  <tr>
                      <th>S.No</th>
                      <th>Product Name</th>
                      <th>Batch</th>
                      <th>Quantity</th>
                      <th>investment</th>
                      <th>Rate</th>
                      @if(Gate::check('isAdmin') || Gate::check('isManager'))
                      <th>Action</th>
                          @endif
                  </tr>
                </thead>
                <tbody>
                @foreach($purchases as $key => $purchase)
                  <tr>
                      <td>{{++$key}}</td>
                      <td>{{\App\Product::find($purchase->product_id)->name}}</td>
                      <td>{{$purchase->batch}}</td>
                      <td>{{$purchase->quantity}}</td>
                      <td>{{$purchase->investment}}</td>
                      <td>{{$purchase->rate}}</td>
                      @if(Gate::check('isAdmin') || Gate::check('isManager'))
                     <td><a href="{{route('purchase.edit',$purchase->id)}}" class="btn btn-primary"><i class="far fa-edit"></i></a>
                         <form action="{{route('purchase.destroy',$purchase->id)}}" method="post" style="display: inline-block">
                             @csrf
                             @method('DELETE')
                             <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                         </form>
                      </td>
                          @endif
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {
            $("[name='select1']").on("change", function () { //listen for change event on the select
                $(".investment").toggle(this.value === "investment");//toggle show/hide based on selected value
                $(".rate").toggle(this.value === "rate");
            }).change();
        });
    </script>


    @endsection