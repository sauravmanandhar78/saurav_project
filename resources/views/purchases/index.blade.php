@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2 list_table_stock ">
            <table class="table table-hover">
                <tr>
                    <th>S.No.</th>
                    <th>Stock ID</th>
                    <th>Batch No</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Action</th>


                </tr>
                @foreach($purchases as $key => $purchase)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$purchase->stock_id}}</td>
                        <td>{{$purchase->batch_no}}</td>
                        <td>{{\App\Product::find($purchase->product_id)->product_name}}</td>
                        <td>{{$purchase->quantity}}</td>
                        <td>{{$purchase->rate}}</td>
                        <td>
                            <a href="{{route('purchase.edit',$purchase->id)}}" class="btn btn-info"><i class="far fa-edit"></i></a>
                            <a href="{{route('product.destroy',$purchase->id)}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>






    @endsection