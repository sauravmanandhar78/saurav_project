@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Sale Update</span>
        </div>
        <br class="card-body">
            <form action="{{route('list.update',$sales->id)}}" method="post" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="product_name">Product</label>
                    <select name="product_id" id="product_id" class="form-control">
                        <option>Select Product</option>
                        @foreach($products as $product)
                            <option value="{{$product->id}}" {{$product->id == $sales->product_id ? 'selected':''}}>{{$product->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-horizontal">
                    <label for="quantity">Quantity</label>
                    <input type="text" name="quantity"  placeholder="Enter Quantity" class="form-control" value="{{$sales->quantity}}">
                    <a href="" style="color: red;">{{$errors->first('quantity')}}</a>
                </div>
                </br>
                <button class="btn btn-primary">Update</button>
            </form>
        </div>




@endsection