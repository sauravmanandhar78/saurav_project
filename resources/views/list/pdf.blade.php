<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        table, th, td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
<h1 style="text-align: center">Inventory Management System</h1>
<h3 style="text-align: center">Naxal, Kathmandu</h3>
<div style="margin-left: 100px;">
    <table style="text-align: center; width: 500px;">
        <tr>
            <th>Product Name</th>
            <th> Quantity</th>
            <th>Rate</th>
            <th>Total</th>
        </tr>
        <tr>
            <td>{{\App\Product::find($pd->product_id)->name}}</td>
            <td>{{$pd->quantity}}</td>
            <td>{{$pd->rate}}</td>
            <td>{{$pd->total}}</td>
        </tr>
    </table>
</div>
</body>
</html>