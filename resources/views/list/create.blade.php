@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px">Sale Transaction</span>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table_id">
                <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Total</th>
                    @if(Gate::check('isAdmin') || Gate::check('isManager'))
                        <th>Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($sales as $key => $sale)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{\App\Product::find($sale->product_id)->name}}</td>
                        <td>{{$sale->quantity}}</td>
                        <td>{{$sale->rate}}</td>
                        <td>{{$sale->total}}</td>
                        @if(Gate::check('isAdmin') || Gate::check('isManager'))
                            <td>{{--<a href="{{route('list.edit',$sale->id)}}" class="btn btn-primary"><i class="far fa-edit"></i></a>--}}
                                <form action="{{route('list.destroy',$sale->id)}}" method="post" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                </form>
                                {{--<a href="{{action('listController@downloadPDF',$sale->id)}}" class="btn btn-primary"><i class="fas fa-file-pdf"></i></a>--}}

                            </td>

                        @endif
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>



    @endsection