@extends('layouts.master')
@section('content')

    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Product Edit</span>
        </div>
        <div class="card-body">
            <form action="{{route('product.update',$products->id)}}" method="post" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" name="name" class="form-control" value="{{$products->name}}">
                    <a href="" style="color: red;">{{$errors->first('name')}}</a>
                </div>
                <div class="form-group">
                    <label for="brand_name">Brands</label>
                    <select name="brand_id" id="brand_id" class="form-control">
                        <option>Select Brand</option>
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}" {{$brand->id == $products->brand_id ? 'selected':''}}>{{$brand->name}}</option>
                            @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories_name">Categories</label>
                    <select name="categorie_id" id="categorie_id" class="form-control">
                        <option>Select Categories</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" {{$category->id == $products->categorie_id ? 'selected':''}}>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>



    @endsection