@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 bord">
            <form action="{{route('check',$stock_data->id)}}" method="post" enctype="multipart/form-data" class="forms">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <select name="product_id" id="product_id" class="form-control">
                        <option>Select Product:</option>
                        @foreach($products_data as $product)
                            <option value="{{$product->id}}" {{$product->id == $stock_data->product_id ? 'selected':''}}>{{$product->product_name}}</option>
                        @endforeach

                    </select>
                </div>
               <div class="form-group">
                    <label for="product_quantity">Quantity:</label>
                    <input type="text" name="product_quantity" id="product_quantity_id" class="form-control" value="{{$stock_data->product_quantity}}">
                </div>


                <div class="form-group">
                    <label for="product_price">Price</label>
                    <input type="text" name="product_price" id="product_price_id" class="form-control" value="{{$stock_data->product_price}}">

                </div>
                <div class="form-group">
                    <button class="btn btn-primary col-md-12" type="submit">Update</button>
                </div>


            </form>


        </div>
    </div>






    @endsection


