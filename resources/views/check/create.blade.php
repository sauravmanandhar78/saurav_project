@extends('layouts.master')
@section('content')
    <style>
        label{
            color: black;
        }
    </style>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <form class="form-horizontal" action="{{route('check')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group ">
                                <label for="stock_id">Stock ID</label>
                                    <input type="text" name="stock_id" id="stock_id" class="form-control">
                            </div>
                            <div class="form-group ">
                                <label for="product_name">Product Name</label>
                                <input type="text" name="product_name" id="product_name" class="form-control">
                            </div>


                            <div class="form-group">
                                <label for="category_name">Category Name</label>
                                <select name="categories_id" id="categories_id" class="form-control">
                                    <option>Select Category:</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="brand_name">Brand Name</label>
                                <select name="brand_id" id="brand_id" class="form-control">
                                    <option>Select Brand:</option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" name="image" id="imageId" class="btn btn-default">
                            </div>
                            <div class="form-group">
                                <label for="product_quantity">Quantity:</label>
                                <input type="text" name="product_quantity" id="product_quantity_id" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="product_price">Price</label>
                                <input type="text" name="product_price" id="product_price_id" class="form-control">
                            </div>

                                <button class="btn btn-primary">Add</button>


                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {{--<style>
        label{
            color: black;
        }
    </style>
<div class="row">
    <div class="col-md-4 col-md-offset-4 bord">
        <form action="{{route('check.store')}}" method="post" enctype="multipart/form-data" class="forms">
            @csrf
            <div class="form-group">
                <label for="name">Product Name</label>
                <select name="product_id" id="product_id" class="form-control">
                    <option>Select Product:</option>
                    @foreach($products as $product)
                        <option value="{{$product->id}}">{{$product->product_name}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="brand_name">Brand Name</label>
                <select name="brand_id" id="brand_id" class="form-control">
                    <option>Select Brand:</option>
                    @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="category_name">Category Name</label>
                <select name="categories_id" id="categories_id" class="form-control">
                    <option>Select Category:</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="stock_id">StockID</label>
                <input type="text" name="stock_id" id="stock_id" class="form-control">
            </div>
            <div class="form-group">
                <label for="product_quantity">Quantity:</label>
                <input type="text" name="product_quantity" id="product_quantity_id" class="form-control">
            </div>
            <div class="form-group">
                <label for="brand">Brand</label>
                <input type="text" name="brand" id="brand" class="form-control">
            </div>
            <div class="form-group">
                <label for="product_price">Price</label>
                <input type="text" name="product_price" id="product_price_id" class="form-control">
            </div>
            <div class="form-group">
                <button class="btn btn-primary col-md-12">Add</button>
            </div>


        </form>
        <div class="form-group">
            <a href="{{route('check.index')}}" class="btn btn-primary col-md-12" style="margin-top: 10px">List Stocks</a>
            </div>

    </div>
</div>


--}}


    @endsection