
<table class="table" id="table">
    <thead>
    <tr>
        <th class="text-center" >S.N</th>
        <th class="text-center" >Product Name</th>
        <th class="text-center" >Quantity</th>
        <th class="text-center" > Rate</th>
        <th class="text-center" > Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sales as $sale)
        <tr>
            <th class="text-center" scope="row">{{$loop->iteration}}</th>
            <td class="text-center" >{{\App\Product::find($sale->product_id)->name}}</td>
            <td class="text-center">{{$sale->quantity}}</td>
            <td class="text-center">{{$sale->rate}}</td>
            <td class="text-center">{{$sale->total}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td class="text-center"><strong>Total Amount:</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-center" style="color: red">{{$sales->sum('total')}}</td>
    </tr>
    </tfoot>
</table>

