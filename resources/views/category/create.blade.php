@extends('layouts.master')
@section('content')
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
            });
        });
    </script>
    @if(count($errors)>0)
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" id="message">
                {{$error}}
            </div>
        @endforeach
    @endif
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal col-md-12"  action="{{route('category.store')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input type="text" name="name" id="name_id" class="form-control">
                            <a href="" style="color: red;">{{$errors->first('name')}}</a>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary col-md-12">Add</button>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-warning col-md-12" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Category</span>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float: right">
                <i class="fas fa-plus"></i>  Add Category
            </button>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table" data-page-length='10'>
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>Category Name</th>
                    @if(Gate::check('isAdmin') || Gate::check('isManager'))
                    <th>Action</th>
                    @endcan
                </tr>
                </thead>

                <tbody>
                @foreach($categories as $key=> $category)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$category->name}}</td>
                        @if(Gate::check('isAdmin') || Gate::check('isManager'))
                        <td>
                            <a href="{{route('category.edit',$category->id)}}" class="btn btn-primary"><i class="far fa-edit"></i></a>
                            <form action="{{route('category.destroy',$category->id)}}" method="post" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                            </form>
                        </td>
                            @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection