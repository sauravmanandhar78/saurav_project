@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px">Account</span>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table_id">
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>User Type</th>
                    <th>Address</th>
                    <th>Gender</th>
                    <th>Image</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($accounts as $key => $account)
                    @if($account -> user_type !== 'admin')
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$account->first_name}}</td>
                        <td>{{$account->last_name}}</td>
                        <td>{{$account->user_type}}</td>
                        <td>{{$account->address}}</td>
                        <td>{{$account->gender}}</td>
                        <td><img src="{{url('lib/images/'.$account->image)}}" height="20" width="30"></td>
                        <td>{{$account->email}}</td>
                        <td>{{$account->phone}}</td>
                        <td><a href="{{route('account.edit',$account->id)}}" class="btn btn-primary"><i class="far fa-edit"></i></a>
                            <form action="{{route('account.destroy',$account->id)}}" method="post" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection