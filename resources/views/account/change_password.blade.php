@extends('layouts.master')
@section('content')

<div class="row">
    <div class="card col-md-4 col-md-offset-4" style="margin-top: 90px; border: 1px solid #5969ff;">
        <div class="card-header">
            <h1>Change Password</h1>
        </div>
        <div class="card-body">
            <form action="{{route('change_password_action')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="password">Enter Old Password</label>
                    <input type="password" name="passwordold" id="password" class="form-control">
                    <a href="" style="color: red;">{{$errors->first('passwordold')}}</a>
                </div>
                <div class="form-group">
                    <label for="password">Enter New Password</label>
                    <input type="password" name="password" class="form-control">
                    <a href="" style="color: red;">{{$errors->first('password')}}</a>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Password Confirm</label>
                    <input type="password" name="password_confirmation" id="confirm_password" class="form-control">
                    <a href="" style="color: red;">{{$errors->first('password_confirmation')}}</a>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary col-md-12">Add</button>
                </div>
            </form>
        </div>
    </div>

</div>
      <div class="col-md-4">

      </div>
</div>















    @endsection