@extends('layouts.master')
@section('content')
    <style>
        label{
            color: black;
        }
    </style>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 reg">
            <h2 class="title"> Registration Form</h2>
        <form action="{{route('account.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group col-md-6">
                <label for="first_name">First Name</label>
                <input type="text" name="first_name" class="form-control">
                <a href="" style="color: red;">{{$errors->first('first_name')}}</a>
            </div>
            <div class="form-group col-md-6">
                <label for="last_name">Last Name</label>
                <input type="text" name="last_name" class="form-control">
                <a href="" style="color: red;">{{$errors->first('last_name')}}</a>
                <br>
            </div>
            <div class="form-group col-md-6">
                <label for="address">Address</label>
                <input type="text" name="address" class="form-control">
                <a href="" style="color: red;">{{$errors->first('address')}}</a>
            </div>
            <br>
            <div class="form-group col-md-4">
                <label for="gender">Gender</label>
                <select  name="gender" class="form-control">
                    <option>Select &nbsp; .....</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <a href="" style="color: red;">{{$errors->first('gender')}}</a>
                </select>
                <br>
            </div>

            <div class="form-group col-md-6">

                <label for="email">Email</label>
                <input type="text" name="email" id="email" class="form-control">
                <a href="" style="color: red;">{{$errors->first('email')}}</a>

            </div>

            <div class="form-group col-md-6">

                <label for="phone">Phone Number</label>
                <input type="text" name="phone" id="phone" class="form-control">
                <a href="" style="color: red;">{{$errors->first('phone')}}</a>
            </div>
            <div class="form-group col-md-6">
                <br>
                <label for="image">Image</label>
                <input type="file" name="image" id="image" class="form-control">
                <a href="" style="color: red;">{{$errors->first('image')}}</a>
            </div>
            <div class="form-group col-md-6">
                <br>
                <label for="user_type">UserType</label>
                <select  name="user_type" class="form-control">
                    <option>Select &nbsp; .....</option>
                    <option value="manager">manager</option>
                    <option value="staff">staff</option>
                    <a href="" style="color: red;">{{$errors->first('user_type')}}</a>
                </select>

                <br>
            </div>
            <div class="form-group col-md-6">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" class="form-control">
                <a href="" style="color: red;">{{$errors->first('password')}}</a>
                <br>
            </div>
            <div class="form-group col-md-6">
                <label for="password_confirmation">Password</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                <a href="" style="color: red;">{{$errors->first('password')}}</a>
                <br>
            </div>
            <div class="form-group col-md-12">
                <button class="btn btn-primary col-md-12">Add Record</button>
            </div>

        </form>
        </div>
    </div>


    @endsection