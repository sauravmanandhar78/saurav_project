@extends('layouts.master')
@section('content')

<div class="col-md-5">
    <div class="card">
        <div class="card-header">
            <h1>User Profile</h1>
        </div>
        <div class="card-body">
           <div class="form-group">
               <label for="name">Name</label>
               <label style="margin-left: 75px;">{{Auth::user()->first_name}}&nbsp;{{Auth::user()->last_name}}</label>

           </div>
            <div class="form-group">
                <label  for="address">Address</label>
                <label style="margin-left: 60px;">{{Auth::user()->address}}</label>
            </div>
            <div class="form-group">
                <label for="gender">Gender</label>
                <label style="margin-left: 65px;">{{Auth::user()->gender}}</label>
            </div>
            <div class="form-group">
                <label  for="email">Email</label>
                <label style="margin-left: 80px;">{{Auth::user()->email}}</label>
            </div>
            <div class="form-group">
                <label  for="phone">Phone</label>
                <label style="margin-left: 75px">{{Auth::user()->phone}}</label>
            </div>
        </div>
    </div>
</div>
    @endsection