@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px">Account</span>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table_id">
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Gender</th>
                    <th>Image</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($accounts as $key => $account)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$account->first_name}}</td>
                        <td>{{$account->last_name}}</td>
                        <td>{{$account->address}}</td>
                        <td>{{$account->gender}}</td>
                        <td><img src="{{url('lib/images/'.$account->image)}}" height="20" width="30"></td>
                        <td>{{$account->email}}</td>
                        <td>{{$account->phone}}</td>
                        <td>
                            <a href="{{route('account.edit',$account->id)}}" class="btn btn-info"><i class="far fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @endsection