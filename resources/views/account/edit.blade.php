@extends('layouts.master')
@section('content')
    <style>
        label{
            color: black;
        }
    </style>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 reg">
            <h2 class="title"> Edit Form</h2>
            <form action="{{route('account.update',$account_data->id)}}" method="post" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group col-md-6">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" value="{{$account_data->first_name}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" value="{{$account_data->last_name}}">
                    <br>
                </div>
                <div class="form-group col-md-6">
                    <label for="address">Address</label>
                    <input type="text" name="address" class="form-control" value="{{$account_data->address}}">
                </div>
                <br>
                <div class="form-group col-md-4">
                    <label for="gender">Gender</label>
                    <input type="text" name="gender" class="form-control" value="{{$account_data->gender}}">
                </div>

                <div class="form-group col-md-6">

                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{$account_data->email}}">

                </div>

                <div class="form-group col-md-6">

                    <label for="phone">Phone Number</label>
                    <input type="text" name="phone" id="phone" class="form-control" value="{{$account_data->phone}}">
                </div>
                <div class="form-group col-md-6">
                    <br>
                    <label for="image">Image</label>
                    <input type="file" name="image" id="image" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <br>
                    <img src="{{url('lib/images/'.$account_data->image)}}" style="height: 160px; width: 330px">
                </div>
                <div class="form-group col-md-12">
                    <button class="btn btn-primary col-md-12" type="submit">Edit</button>
                </div>
            </form>
        </div>
    </div>









    @endsection