@extends('layouts.master')
@section('content')

    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Brand Edit</span>
        </div>
        <div class="card-body">
            <form action="{{route('brand.update',$brands->id)}}" method="post" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Brand Name</label>
                    <input type="text" name="name" class="form-control" value="{{$brands->name}}">
                    <a href="" style="color: red;">{{$errors->first('name')}}</a>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>




    @endsection