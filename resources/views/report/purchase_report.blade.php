@extends('layouts.master')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <p>Report</p>
            </div>
            <div class="card-body">
                <div class="col-md-4">
                    <label for="from-date">From Date</label>
                    <input type="text" name="from_date" class="from-date form-control" id="datepicker">
                </div>
                <div class="col-md-4">
                    <label for="to-date">To Date</label>
                    <input type="text" name="to_date" class="to-date form-control" id="datepick">
                </div>
                <button type="submit" style="margin-top: 20px;" class="generate btn btn-primary">Generate Report</button>
                <button id="pdf" style="margin-top: 20px;"  type="submit" class=" pdf btn btn-primary">Download Report</button>

            </div>
            <div class="card-footer">
                <div id="report" class="report">

                </div>
                <div id="editor"></div>
            </div>
        </div>

    </div>
    <script>
        $( function() {
            $( "#datepicker,#datepick" ).datepicker({
                dateFormat: 'yy-mm-dd' })
        });
    </script>
    <script>

        $(document).ready(function () {
            $(".generate").on('click',function () {
                var from_date = $(".from-date").val();
                var to_date = $(".to-date").val();

                var report ={
                    from_date:from_date,
                    to_date:to_date
                };
                $.ajax({
                    type:'GET',
                    url: '{{route('report')}}',
                    data:report,
                    success:function (data) {
                        $('#report').html(data);
                    }
                })
            });
        })
    </script>
    <script>
        $(document).ready(function(){
            var doc = new jsPDF();

// We'll make our own renderer to skip this editor
            var specialElementHandlers = {
                '#editor': function(element, renderer){
                    return true;
                }
            };
            margins ={
                left:30,
            };

            $('#pdf').click(function () {
                doc.fromHTML($('#report').get(0), 25, 15, {
                    'width': 100,
                    'elementHandlers': specialElementHandlers
                });
                doc.save('sample-file.pdf');
            });
        });
    </script>

    @endsection