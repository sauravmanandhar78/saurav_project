@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <table class="table table-hover" style="background-color: white">
                <tr>
                    <th>S.No</th>
                    <th>Product Name</th>
                    <th>Image</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            @foreach($userData as $key=>$userDatum)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$userDatum->product_name}}</td>
                    <td>
                        <img src="{{url('lib/images/'.$userDatum->image)}}" height="20" width="30">
                    </td>
                    <td>{{$userDatum->quantity}}</td>
                    <td>{{$userDatum->price}}</td>
                    <td>
                        <a href="" class="btn btn-info">Edit</a>
                        <a href="" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                @endforeach
            </table>





        </div>
    </div>








    @endsection