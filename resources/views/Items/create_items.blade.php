@extends('layouts.master')
@section('content')
    <style>
        .form-group{
            color: black;
        }
    </style>

    <div class="row">
        <div class="col-md-4 col-md-offset-4 bord">
            <form action="{{route('store_items')}}" method="post" enctype="multipart/form-data" class="forms">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="product_name" style="color: black">Product Name</label>
                    <input type="text" name="product_name" id="productid" class="form-control">
                </div>

                <div class="form-group">
                    <label for="image" style="color: black">Image</label>
                    <input type="file" name="image" id="imageId" class="btn btn-default">
                </div>
                <div class="form-group">
                    <label for="quantity" style="color: black">Quantity</label>
                    <input type="text" name="quantity" id="quantityid" class="form-control">
                </div>
                <div class="form-group">
                    <label for="price" style="color: black">Price</label>
                    <input type="text" name="price" id="priceid" class="form-control">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary col-md-12">Add</button>
                </div>
            </form>
        </div>
    </div>


@endsection