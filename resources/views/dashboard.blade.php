@extends('layouts.master')
@section('content')


   <div class="row">
      <div class="container">
         <div class="col-md-4" style="margin-top: 30px;">
            <div class="card " style="background-color: red">
               <div class="card-body">
                  <h5 style="color: white">Users</h5>
                  <i class="fas fa-users fa-5x" style="float:right; color: white"></i>
                  <div class="metric-value d-inline-block">
                     <h1 class="mb-1" style="color: white">{{\App\User::all()->count()}}</h1>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4" style="margin-top: 30px;">
            <div class="card border-3" style="background-color: #03C03C; color: white" >
               <div class="card-body">
                  <h5 style="color: white">Purchase</h5>
                  <i class="fas fa-rupee-sign fa-5x" style="float: right"></i>
                  <div class="metric-value d-inline-block">
                     <h1 class="mb-1" style="color: white">Rs. {{App\Purchase::sum('investment')}}</h1>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4" style="margin-top: 30px;">
            <div class="card border-3" style="background-color: #56A0D3; color: white">
               <div class="card-body">
                  <h5 style="color: white">Sales</h5>
                  <i class="fas fa-rupee-sign fa-5x" style="float: right"></i>
                  <div class="metric-value d-inline-block">
                     <h1 class="mb-1" style="color: white">Rs. {{App\Sale::sum('total')}}</h1>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
        {{-- <div class="col-md-12" >
            <div class="card border-primary card-outline print" style="border: 1px solid">
               <div class="card-header">
                  <p style="font-size: 20px;">Report</p>
               </div>
               <div  class="card-body">
                  <div class="col-md-4">
                     <label for="from-date">From Date</label>
                     <input type="text"  name="from_date" class="from-date form-control" id="datepicker">
                  </div>
                  <div class="col-md-4">
                     <label for="to-date">To Date</label>
                     <input type="text" name="to_date" class="to-date form-control" id="datepick">
                  </div>

                     <button type="submit" class=" generate btn btn-primary" style="margin-top: 20px;">Generate Report</button>


               </div>
               <div class="card-footer" style="background-color: white">
                   <div class="col-md-3" style="display: inline-block; margin-top: 10px;">
                       <p style="color: red">Note*: To download report, first you have to generate the report.</p>
                       <button id="pdf" type="submit" class=" pdf btn btn-primary">Download Report</button>
                   </div>
                  <div id="report" class="report">

                  </div>
                   <div id="editor"></div>
               </div>
            </div>
         </div>
      <!-- /.content -->

      </div>--}}
   {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.js" ></script>

<script>
    $( function() {
        $( "#datepicker,#datepick" ).datepicker({
         dateFormat: 'yy-mm-dd' })
        });

</script>
   <script type="text/javascript">
       $(document).ready(function(){
           $(".generate").on('click',function() {
               var from_date = $(".from-date").val();
               var to_date = $(".to-date").val();

               var report ={
                   from_date: from_date,
                   to_date: to_date

               };

               $.ajax({
                   type: 'GET',
                   url: '', --}}{{--{{route('report')}}--}}{{--
                   data: report,
                   success: function (data) {
                       $('#report').html(data);
                   }
               });
           });
       });
   </script>
   <script>
       $(document).ready(function(){
          var doc = new jsPDF();


           var specialElementHandlers = {
               '#editor': function(element, renderer){
                   return true;
               }
           };

           $('#pdf').click(function () {
               doc.fromHTML($('#report').get(0), 15, 15, {
                   'width': 500,
                   'elementHandlers': specialElementHandlers
               });

               doc.save('sample-file.pdf');
           });
       });
   </script>
--}}
@endsection