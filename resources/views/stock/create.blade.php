@extends('layouts.master')
@section('content')
    <style>
        td,span{
            color: black;
        }
    </style>

    <div class="card" style="">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Stock</span>
           {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" style="float: right">
                <i class="fas fa-plus"></i>  Add
            </button>--}}
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table">
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    {{--<th>Action</th>--}}
                </tr>
                </thead>
                    <tbody>
                    @foreach($stocks as $key => $stock)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{\App\Product::find($stock->product_id)->name}}</td>
                        <td <?php
                            if($stock->quantity < 10){
                                echo "style =  background-color:#F44336";
                            }

                            ?>>{{$stock->quantity}}</td>
                       {{-- <td>
                            <a href="{{route('stock.edit',$stock->id)}}" class="btn btn-primary"><i class="far fa-edit"></i></a>
                            <a href="" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
                        </td>--}}
                    </tr>
                    @endforeach
                    </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#table').DataTable({
            });
        });
    </script>


    @endsection