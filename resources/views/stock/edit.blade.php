@extends('layouts.master')
@section('content')


    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Stock Edit</span>
        </div>

        <div class="card-body">
            <form action="{{route('stock.update',$stocks->id)}}" method="post" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="product_name">Product</label>
                    <select name="product_id" id="product_id" class="form-control">
                        <option>Select Product</option>
                        @foreach($products as $product)
                            <option value="{{$product->id}}" {{$product->id == $stocks->product_id ? 'selected':''}}>{{$product->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="quantity">Quantity</label>
                    <input type="text" name="quantity" class="form-control" value="{{$stocks->quantity}}">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>






    @endsection