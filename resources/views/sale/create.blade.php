@extends('layouts.master')
@section('content')
<style>
    .round{
        display: block;
        width: 35px;
        height: 35px;
        line-height: 30px;
        color:#f5f5f5;
        border: 2px solid #f5f5f5;
        text-align: center;
        border-radius: 50%;
        text-decoration: none;
        background: #3D5AFE;
    }
    table{
        border: 1px solid #5969ff ;

    }
</style>

    <div class="form-group">
        <form name="add_name" id="add_name">
            @csrf
            <div class="alert alert-danger print-error-msg" style="display: none;"><ul></ul></div>
            <div class="alert alert-success print-success-msg" style="display: none;"><ul></ul></div>
            <br><br>
            <div class="table   table-responsive" style="border: 1px solid #5969ff  ">
                <table class="table table-bordered" id="dynamic_field" border="0">
                    <tr>
                        <th>Product Name</th>
                        <th>Total Quantity</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Sub Total</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                      {{--  <td><input type="text" name="name[]" placeholder="Enter Name" class="form-control name_list"></td>--}}
                        <td><select name="product_id[]" id="product_id" class="form-control">
                                <option>Select Product:</option>
                                @foreach($stocks as $product)
                                    <option value="{{$product->id}}">{{\App\Product::find($product->id)->name}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td><input type="text" name="quantit[]" id="quantities" placeholder="Enter Quantity" class="form-control quantity_list" readonly></td>
                        <td><input type="text" name="quantity[]" id="quantitie"  placeholder="Enter Quantity" class="form-control quantity_list"></td>
                        <td><input type="text" name="rate[]" id="rates" placeholder="Enter Rate" class="form-control rate_list"></td>
                        <td><input type="text" name="total[]"  id="totals" placeholder="Enter Total" class="form-control total_list" readonly></td>
                        <td><input type="text" name="totals[]"  id="totalas" placeholder="Enter Total" class="form-control total_lists"></td>
                        <td><button type="button" name="add" id="add" class="round"><i class="fas fa-plus"></i></button></td>
                    </tr>
                </table>
            </div>
            <input type="button"  style="margin-top: 2px;" name="submit" id="submit" class="btn btn-primary" value="Submit" />
            <div class="form-group" style="margin-top: 100px">
                <label>Total :</label>
                <input type="text" id="income_sum" name="income_sum">
            </div>
        </form>
    </div>



    <script type="text/javascript">
        $(document).ready(function () {
            var postURL="<?php echo route ('addStore')?>";
            var i=1;
            $(document).on('click','#add',function () {
                i++;
                var myurl = "<?php echo route ('partialcreate')?>";
                $.ajax({
                    url:myurl,
                    method:"GET",
                    data:{i : i},
                    success:function (data) {
                        // console.log('data');
                        $("#dynamic_field").append(data);

                    }

                });


            });

            $(document).on('click','.btn_remove',function () {
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
            $("#quantitie,#rates").on('keyup',function () {
                $('#totals').val($('#quantitie').val() * $('#rates').val());
            });


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#submit').click(function () {
                $.ajax({
                    url:postURL,
                    method:"POST",
                    data:$('#add_name').serialize(),
                    type:'json',
                    success:function (data) {
                        console.log(data);
                        if(data.error){
                            printErrorMsg(data.error);
                        }else{
                            i=1;
                            $('.dynamic-added').remove();
                            $('#add_name')[0].reset();
                            $(".print-success-msg").find("ul").html('');
                            $(".print-success-msg").css('display','block');
                            $(".print-error-msg").css('display','none');
                            $(".print-success-msg").find("ul").append('<li>Record Inserted.</li>');
                        }
                    }

                });
            });


            function printErrorMsg(msg) {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','block');
                $(".print-success-msg").css('display','none');
                $.each(msg, function (key, value) {
                    $(".print-error-msg").find("ul").append(/*'<li>'+value+'</li>'*/'Please Fill the form properly');
                });
            }
        });


    </script>
    <script>
        $(document).ready(function () {
            $("#product_id").on("change",function () {
                let product = $("#product_id").val();
                let product_id = {
                    product_id: product,
                };
                $.ajax({
                    type:"GET",
                    url:"{{route('getproducts')}}",
                    data:product_id,
                    dataType:"json",
                    success:function (data) {
                        $("#quantities").val(data.quantity);
                    }
                });

            });
        });
    </script>
<script>
   var $form = $('#add_name'),
       $sumDisplay = $('#income_sum');
   $form.delegate('.total_lists','keyup',function () {
      var $summ = $form.find('.total_lists');
      var sum =0;
      $summ.each(function () {
          var value = Number($(this).val());
          if(!isNaN(value)) sum += value;
      });
      $sumDisplay.val(sum);
   });

</script>
@endsection