@if(session('success'))
    <div class="alert alert-success" id="message">
        {{session('success')}}
    </div>
@endif

<script>
    setTimeout(function () {
        $('#message').fadeOut('fast');
    },3000);
</script>
