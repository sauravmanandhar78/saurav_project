<body>
<!-- ============================================================== -->
<!-- main wrapper -->
<!-- ============================================================== -->
<div class="dashboard-main-wrapper">
    <!-- ============================================================== -->
    <!-- navbar -->
    <!-- ============================================================== -->
    <div class="dashboard-header">
        <nav class="navbar navbar-expand-lg bg-white fixed-top">
            <a class="navbar-brand" href="{{url('dashboard')}}">Inventory</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-right-top">



                    <li class="nav-item dropdown nav-user">
                        @if(isset(Auth::user()->email))
                        <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->email}}</a>
                         @else
                            <script>window.location="/";</script>
                        @endif
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                          @if(Gate::check('isAdmin') || Gate::check('isManager'))
                            <a class="dropdown-item" href="{{route('account.index')}}"><i class="fas fa-user mr-2"></i>Account</a>
                         @endcan
                           {{-- <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a>--}}

                            <a class="dropdown-item" href="{{url('/logout')}}"><i class="fas fa-power-off mr-2"></i>Logout</a>

                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- ============================================================== -->
    <!-- end navbar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- left sidebar -->
    <!-- ============================================================== -->
    <div class="nav-left-sidebar sidebar-dark">
        <div class="menu-list">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="d-xl-none d-lg-none" href="{{url('/dashboard')}}">Dashboard</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav flex-column">
                        <li class="nav-divider">
                            Home
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link active" href="{{url('/dashboard')}}"  aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i>Dashboard <span class="badge badge-success">6</span></a>
                           {{-- <div id="submenu-1" class="collapse submenu" style="">
                                <ul class="nav flex-column">

                                    <li class="nav-item">
                                        <a class="nav-link" href="dashboard-finance.html">Finance</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="dashboard-sales.html">Sales</a>
                                    </li>

                                </ul>
                            </div>--}}
                        </li>




                        <li class="nav-divider">
                            Menu
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('category.create')}}"   aria-expanded="false" {{--data-target="#submenu-6" aria-controls="submenu-6"--}}><i class="fas fa-user"></i> Category </a>
                            {{--<div id="submenu-6" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/blank-page.html">Profile</a>
                                    </li>

                                </ul>
                            </div>--}}
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('brand.create')}}"   aria-expanded="false" {{--data-target="#submenu-6" aria-controls="submenu-6"--}}><i class="fas fa-user"></i> Brand </a>
                            {{--<div id="submenu-6" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/blank-page.html">Profile</a>
                                    </li>

                                </ul>
                            </div>--}}
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{route('stock.create')}}"   aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-6"><i class="fas fa-user"></i> Stock </a>
                            {{--<div id="submenu-6" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/blank-page.html">Profile</a>
                                    </li>

                                </ul>
                            </div>--}}
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" href="{{route('product.create')}}" aria-expanded="false" data-target="#submenu-7" aria-controls="submenu-7"><i class="fas fa-folder"></i>Products<span class="badge badge-secondary">New</span></a>
                            {{--<div id="submenu-7" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="">Items</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/email-details.html">Categories</a>
                                    </li>

                                </ul>
                            </div>--}}
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('purchase.create')}}"  aria-expanded="false" data-target="#submenu-8" aria-controls="submenu-8"><i class="fas fa-fw fa-inbox"></i>Purchase <span class="badge badge-secondary">New</span></a>
                            {{--<div id="submenu-8" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/inbox.html">Daily Report</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/email-details.html">Weekly Report</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="pages/email-compose.html">Monthly Report</a>
                                    </li>

                                </ul>
                            </div>--}}
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="" data-toggle="collapse"  aria-expanded="false" data-target="#submenu-9" aria-controls="submenu-9"><i class="fas fa-fw fa-inbox"></i>Sales <span class="badge badge-secondary">New</span></a>
                            <div id="submenu-9" class="collapse submenu">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('sale/create')}}">Sale Item</a>
                                    </li>
                                </ul>
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('list.create')}}">Sale List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="" data-toggle="collapse"  aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-9"><i class="fas fa-fw fa-inbox"></i>Report<span class="badge badge-secondary">New</span></a>
                            <div id="submenu-3" class="collapse submenu">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('report/purchase_report')}}">Purchase Report</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('report/sale_report')}}">Sales Report</a>
                                    </li>
                                </ul>
                            </div>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" href="{{url('logout')}}" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-cog"></i> Settings</a>
                            <div id="submenu-5" class="collapse submenu" style="">
                                <ul class="nav flex-column">


                                   <li class="nav-item">
                                        <a class="nav-link" href="{{route('account.create')}}">Create Account</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('setting/change_password')}}">Change Password</a>
                                    </li>

                                </ul>
                            </div>
                        </li>




                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end left sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
    <div class="dashboard-wrapper">
