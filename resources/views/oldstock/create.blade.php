@extends('layouts.master')
@section('content')


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLable" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Stock</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal col-md-12" action="{{route('oldstock')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group ">
                        <label for="id">Stock ID</label>
                        <input type="text" name="id" id="id" class="form-control">
                    </div>
                    <div class="form-group ">
                        <label for="product_name">Product Name</label>
                        <input type="text" name="product_name" id="product_name" class="form-control">
                    </div>


                    <div class="form-group">
                        <label for="category_name">Category Name</label>
                        <select name="categories_id" id="categories_id" class="form-control">
                            <option>Select Category:</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="brand_name">Brand Name</label>
                        <select name="brand_id" id="brand_id" class="form-control">
                            <option>Select Brand:</option>
                            @foreach($brands as $brand)
                                <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="imageId" class="btn btn-default">
                    </div>
                    <div class="form-group">
                        <label for="product_quantity">Quantity:</label>
                        <input type="text" name="product_quantity" id="product_quantity_id" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="product_price">Price</label>
                        <input type="text" name="product_price" id="product_price_id" class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary col-md-12">Add</button>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning col-md-12" data-dismiss="modal">Close</button>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>

    <div class="card">
        <div class="card-header border-2 border-top border-top-primary border-primary">
            <span style="font-size: 20px;">Stock</span>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float: right">
                <i class="fas fa-plus"></i>  Add Stock
            </button>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table_id" data-page-length='5'>
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>id</th>
                    <th>Product Name</th>
                    <th>Categories</th>
                    <th>Brand</th>
                    <th>Image</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($stocks as $key=> $stock)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$stock->id}}</td>
                        <td>{{$stock->product_name}}</td>
                        <td>{{\App\Category::find($stock->categories_id)->category_name}}</td>
                        <td>{{\App\Brand::find($stock->brand_id)->brand_name}}</td>
                        <td>
                            <img src="{{url('lib/images/'.$stock->image)}}" height="20" width="30">
                        </td>
                        <td>{{$stock->product_quantity}}</td>
                        <td>{{$stock->product_price}}</td>
                        <td>
                            <a href="{{route('stock.edit',$stock->id)}}" class="btn btn-primary"><i class="far fa-edit"></i></a>
                            <a class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
     </div>









    @endsection