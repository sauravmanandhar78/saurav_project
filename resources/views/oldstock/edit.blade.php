@extends('layouts.master')
@section('content')
<style>
    label{
        color: black;
    }

</style>
<div class="card">
    <div class="card-header border-2 border-top border-top-primary border-primary">
        <span style="font-size: 20px;">Stock</span>
    </div>
    <div class="card-body">
        <form action="{{route('oldstock',$stock_data->id)}}" method="post" enctype="multipart/form-data" class="forms">
            @method('PATCH')
            @csrf
            <div class="form-group ">
                <label for="id">Stock ID</label>
                <input type="text" name="id" id="id" class="form-control" value="{{$stock_data->id}}">
            </div>
            <div class="form-group ">
                <label for="product_name">Product Name</label>
                <input type="text" name="product_name" id="product_name" class="form-control" value="{{$stock_data->product_name}}">
            </div>
            <div class="form-group">
                <label for="category_name">Category Name</label>
                <select name="categories_id" id="categories_id" class="form-control">
                    <option>Select Category:</option>
                    @foreach($categories_data as $category)
                        <option value="{{$category->id}}" {{$category->id == $stock_data->categories_id ? 'selected':''}} >{{$category->category_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="brand_name">Brand Name</label>
                <select name="brand_id" id="brand_id" class="form-control">
                    <option>Select Brand:</option>
                    @foreach($brand_data as $brand)
                        <option value="{{$brand->id}}" {{$brand->id == $stock_data->brand_id ? 'selected':''}}>{{$brand->brand_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="image">Image</label>
                <input type="file" name="image" id="imageId" class="btn btn-default">
            </div>
            <div class="form-group">


                    <img src="{{url('lib/images/'.$stock_data->image)}}" class="img-responsive thumbnail" style="height: 150px; width: 290px">


            </div>
            <div class="form-group">
                <label for="product_quantity">Quantity:</label>
                <input type="text" name="product_quantity" id="product_quantity_id" class="form-control" value="{{$stock_data->product_quantity}}">
            </div>
            <div class="form-group">
                <label for="product_price">Price</label>
                <input type="text" name="product_price" id="product_price_id" class="form-control" value="{{$stock_data->product_price}}">
            </div>
            <div class="form-group">
                <button class="btn btn-primary col-md-12">Add</button>
            </div>

        </form>
    </div>
</div>
@endsection