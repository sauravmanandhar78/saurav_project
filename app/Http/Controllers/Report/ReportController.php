<?php

namespace App\Http\Controllers\Report;

use App\Product;
use App\Purchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use PDF;

class ReportController extends Controller
{
    public function report(Request $request){
        $fromdate = $request->get('from_date');
        $todate = $request->get('to_date');
        $data['purchases'] = Purchase::whereBetween('created_at', [$fromdate, $todate])->get();
        /*$data['purchases'] = Purchase::where('created_at' ,'>=',$fromdate)
        ->where('created_at','<=',$todate)
        ->get();*/
        return view('report',$data);
    }
   public function index(){
       return view('report.purchase_report');
   }
    function get_purchase_data()
    {

        $customer_data = DB::table('purchases')
            ->get();
        return $customer_data;
    }

    function pdf()
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_customer_data_to_html());
        return $pdf->stream();
    }

    function convert_customer_data_to_html()
    {
        $customer_data = $this->get_purchase_data();
        $output = '
    
     <h3 align="center">Inventory Management System</h3>
     <p style="margin-left: 300px; font-weight: bold">Naxal, Kathmandu</p>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Product Name</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Batch</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Quantity</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Investment</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Rate</th>
   </tr>
     ';
        foreach($customer_data as $customer)
        {
            $output .= '
      <tr>
       <td style="border: 1px solid; padding: 12px">'.Product::find($customer->product_id)->name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->batch.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->quantity.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->investment.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->rate.'</td>
      </tr>
      ';
        }
        $output .= '</table>';
        return $output;
    }

}
