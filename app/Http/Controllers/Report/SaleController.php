<?php

namespace App\Http\Controllers\Report;

use App\Product;
use App\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use PDF;

class SaleController extends Controller
{
    public function index(){
        return view('report.sale_report');
    }
    public function sale_report(Request $request){
        $fromdate = $request->get('from_date');
        $todate = $request->get('to_date');
        $data['sales'] = Sale::whereBetween('created_at',[$fromdate,$todate])->get();
        return view('sale_report',$data);
    }
    public function get_sale_data(){
        $sale = DB::table('sales')
            ->limit(10)
            ->get();
        return $sale;
    }
    function pdf(){
       $pdf = \App::make('dompdf.wrapper');
       $pdf->loadHTML($this->convert_sale_data_to_html());
       return $pdf->stream();
    }
    function convert_sale_data_to_html(){
       $sale = $this->get_sale_data();
       $output = '
       <h3 align="center">Inventory Management System</h3>
       <table width="100%" style="border-collapse: collapse; border=0px;">
       <tr>
          <th>Product Name</th>
          <th>Quantity</th>
          <th>Rate</th>
          <th>Total</th>
       </tr>
     
       
       ';
       foreach ($sale as $sa){
           $output .= '
           <tr>
           <td style="border:1px solid; padding: 12px; ">'.Product::find($sa->product_id)->name.'</td>
            <td style="border:1px solid; padding: 12px; ">'.$sa->quantity.'</td>
            <td style="border:1px solid; padding: 12px; ">'.$sa->rate.'</td>
            <td style="border:1px solid; padding: 12px; ">'.$sa->total.'</td>
           </tr>
           ';
       }
       $output .= '</table>';
       return $output;
    }
}
