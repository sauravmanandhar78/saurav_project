<?php

namespace App\Http\Controllers\Purchase;

use App\Category;
use App\Product;
use App\Purchase;
use App\OldStock;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $purchases = Purchase::all();
       return view('purchases.index',compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['products'] = Product::all();
        $data['purchases'] = Purchase::all();
        $data['categories'] = Category::all();
        return view('purchases.create',$data);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'product_id' => 'required|exists:products,id',
            'quantity'=>'required|integer|min:1',

        ]);
        $min=1;
        $max=10000;
        $product_id = $request->get('product_id');
        $investment = $request->get('investment');
        $quantity=$request->get('quantity');
        $rate = $request->get('rate');
        if ($investment != null){
            $rate = $investment/$quantity;
        }
        elseif ($rate != null){
            $investment = $rate*$quantity;
        }
        $purchase = Purchase::where('product_id',$product_id)->where('investment',$investment)->first();
        if(isset($purchase)){

            $purchase = new Purchase([
                'product_id' => $product_id,
                'batch' =>$purchase->batch,
                'quantity' => $quantity,
                'investment' => $investment,
                'rate' => $rate,
            ]);
            $purchase->save();
        }
        else{
            $purchase = new Purchase([
                'product_id' => $product_id,
                'batch' => rand($min,$max),
                'quantity'=> $request->get('quantity'),
                'investment' => $investment,
                'rate' => $rate,
            ]);
            $purchase->save();
        }

        $stock = Stock::where('product_id','=',$product_id)->first();

        if (isset($stock)){
            DB::table('stocks')
                ->where('product_id',$product_id)
                ->increment('quantity',$purchase->quantity);
        }
        else{
            Stock::create([
                'product_id' => $request->get('product_id'),
                'quantity' => $request->get('quantity'),
            ]);
        }




        return redirect()->route('stock.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['products'] = Product::all();
        $data['purchases'] = Purchase::find($id);
        return view('purchases.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'quantity'=>'required|integer|min:1',
            'investment'=>'required|integer|min:1',
            'rate'=>'required|integer|min:1'
        ]);
       $purchase = Purchase::FindOrfail($id);
       $quantity = $request->get('quantity');
       $quantity = (int)$quantity;
        $product_id = $request->get('product_id');
        $stock = Stock::where('product_id','=',$product_id)->first();
        if (isset($stock)){
            DB::table('stocks')
                ->where('product_id',$product_id)
                ->decrement('quantity',$purchase->quantity);

            DB::table('stocks')
                ->where('product_id',$product_id)
                ->increment('quantity',$quantity);
        }

        $purchase['product_id'] = $request->get('product_id');
        $purchase['quantity'] = $request->get('quantity');
        $purchase['investment'] = $request->get('investment');
        $purchase['investment'] = $request->get('investment');
        $purchase->save();





        return redirect()->route('purchase.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Purchase::findOrFail($id);

        $data->delete();
        return redirect()->route('purchase.create');
    }
}
