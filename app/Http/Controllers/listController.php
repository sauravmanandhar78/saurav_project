<?php

namespace App\Http\Controllers;

use App\Product;
use App\Sale;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class listController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['sales'] = Sale::all();
        return view('list.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['products'] = Product::all();
        $data['sales'] = Sale::find($id);
        return view('list.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'quantity'=>'required|integer|min:1',
        ]);
        $sales = Sale::findOrFail($id);
        $quantity = $request->get('quantity');
        $quantity = (int)$quantity;
        $product_id = $request->get('product_id');
        $stock = Stock::where('product_id','=',$product_id)->first();
        if (isset($stock)){
            DB::table('stocks')
                ->where('product_id',$product_id)
                ->decrement('quantity',$sales->quantity);

            DB::table('stocks')
                ->where('product_id',$product_id)
                ->increment('quantity',$sales->quantity);
        }

        $sales['product_id'] = $request->get('product_id');
        $sales['quantity'] = $request->get('quantity');
        $sales->save();
        return redirect()->route('list.create')->with('success','Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Sale::findOrFail($id);
        $data->delete();
        return redirect()->route('list.create');
    }
    public function downloadPDF($id){
        $pd = Sale::find($id);
        $pdf = PDF::loadView('list.pdf',compact('pd'));
        return $pdf->download('invoice.pdf');
    }
    public function list(){
        return view('test');
    }
}
