<?php

namespace App\Http\Controllers\Setting;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class SettingController extends Controller
{
    public function index(){
        return view('account.change_password');
    }
    public function create_profile(){
        $profiles = User::all();
        return view('account.create_profile',compact('profiles'));
    }
    public function change_password_action(Request $request){
        $this->validate($request, [
            'password' => 'confirmed|min:6|alpha_num',
        ]);
        $User = User::find(Auth::user()->id);
        if(Hash::check(Input::get('passwordold'), $User['password']) && Input::get('password') == Input::get('password_confirmation')){
            $User->password = bcrypt(Input::get('password'));
            $User->save();
            return back()->with('success','Password Changed');
        }
        else{
            return back()->with('error','Password Not Changed');
        }

    }
}
