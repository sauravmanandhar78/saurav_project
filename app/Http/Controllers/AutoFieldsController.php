<?php

namespace App\Http\Controllers;

use App\AutoField_model;
use App\Sale;
use Illuminate\Http\Request;
use Validator;

class AutoFieldsController extends Controller
{
    public function addMore(){
        return view('addFields');
    }
    public function addMorePost(Request $request){
        $rules=[];
        foreach ($request->input('name') as $key=>$value){
            $rules["name.{$key}"]='required';
            $rules["address.{$key}"]='required';
        }
        $validator = Validator::make($request->all(),$rules);
        $add=$request->get('address');
        if($validator->passes()){

            foreach ($request-> input('name') as $key => $value){
                AutoField_model::create(['name'=>$value, 'address'=>$request->address[$key]]);
            }


            return response()->json(['success='>'done']);
        }
        else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
}
