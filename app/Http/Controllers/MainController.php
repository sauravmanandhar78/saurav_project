<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class MainController extends Controller
{
    public function index(){
        return view('first');
    }
     public function _construct(){
        $this->middleware('auth');
     }
    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postlogin(Request $request){
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ]);
        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );
        if(Auth::attempt($user_data)){
            return redirect('/dashboard');
        }
        else{
            return back()->with('error','Wrong Login Details');
        }
    }

    public function homePage(){
        return view('dashboard');
    }
    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
