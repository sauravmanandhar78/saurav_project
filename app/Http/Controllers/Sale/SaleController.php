<?php

namespace App\Http\Controllers\Sale;

use App\Product;
use App\Purchase;
use App\Sale;
use App\Stock;
use App\User;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['products'] = Product::all();
        $data['stocks'] = Stock::all();
        return view('sale.create',$data);
    }
    public function list(){
       $data['sales'] = Sale::all();
       return view('sale.list',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addStore(Request $request)
    {
        $rules=[];
        foreach ($request->input('product_id') as $key=>$value){
       /*     $rules["name.{$key}"]='required';*/
            $rules["quantity.{$key}"]='required|integer|min:1';
            $rules["product_id.{$key}"]='required|integer|min:1';
            $rules["rate.{$key}"]='required|integer|min:1';
        }
        $validator = Validator::make($request->all(),$rules);
        /*$add=$request->get('address');*/
        if($validator->passes()){

            foreach ($request-> input('product_id') as $key => $value){
                Sale::create([/*'product_id'=>$value*/ 'product_id'=>$request->product_id[$key], 'quantity'=>$request->quantity[$key],'rate'=>$request->rate[$key],'total'=>$request->total[$key]]);
                $product_id =$request->product_id[$key];
                $stock = Stock::where('product_id','=',$product_id)->first();
                if (isset($stock)){
                    DB::table('stocks')
                        ->where('product_id',$product_id)
                        ->decrement('quantity',$request->quantity[$key]);

                }
            }

            return response()->json(['success='>'done']);


        }
        else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }



    }
    public function getproducts(Request $request){
        $id = $_GET['product_id'];
        $data = Stock::where('id',$id)->findOrFail($id);
        if(!empty($data)){
            $response = ['quantity' => $data -> quantity];
            echo json_encode($response);
        }
        else{
            echo 'not set';
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }

    public function partialcreate(Request $request)
    {
        $i = $_GET['i'];
        $data['products'] = Product::all();
        $data['stocks'] = Stock::all();
        $data['i'] = $i;
        return view('sale.partial_create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
