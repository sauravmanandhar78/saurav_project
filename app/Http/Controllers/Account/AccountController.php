<?php

namespace App\Http\Controllers\Account;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = User::all();
        return view('account.index',compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'address' => 'required|alpha',
            'gender' => 'required|alpha',
            'email' => 'required',
            'image' => 'required',
            'phone' => 'required|min:10',
            'user_type' => 'required|alpha',
            'password' => 'required|confirmed|alpha_num|min:5'
        ]);
        $user = new User();
           $user['first_name'] = $request->get('first_name');
            $user['last_name'] = $request->get('last_name');
            $user['address']  = $request->get('address');
            $user['gender']  = $request->get('gender');
            $user['email']  = $request->get('email');
            $user['phone'] = $request->get('phone');
            $user['user_type'] = $request->get('user_type');
            $user['password'] = bcrypt($request->get('password'));


        if($request->hasFile('image')){
            $image = $request->file('image');
            $ext = $image->getClientOriginalExtension();
            $imageName = str_random().'.'.$ext;
            $uploadPath = public_path('lib/images/');
            $image->move($uploadPath, $imageName);
            $user['image'] = $imageName;

        }

        $user->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account_data = User::find($id);
        return view('account.edit',compact('account_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var TYPE_NAME $this */
        $this->validate($request,[
            'image' => 'mimes:jpeg,jpg,png,gig'
        ]);
        $account = User::findorFail($id);
        $account['first_name']=$request->first_name;
        $account['last_name']=$request->last_name;
        $account['address']=$request->address;
        $account['gender']=$request->gender;
        $account['email']=$request->email;
        $account['phone']=$request->phone;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $ext = $image->getClientOriginalExtension();
            $imageName = str_random().'.'.$ext;
            $uploadPath = public_path('lib/images/');
            if($image->move($uploadPath, $imageName)){
                $account['image'] = $imageName;
            }

        }
        $account->save();
        return redirect('/account');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $user = User::findOrFail($id);
            $user->delete();
            return redirect()->route('account.create')->with('success','Account Deleted');
        }
}
