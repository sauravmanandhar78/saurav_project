<?php

namespace App\Http\Controllers\Product;

use App\Brand;
use App\Category;
use App\Product;
use App\OldStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['products'] = Product::all();
        $data['brands'] = Brand::all();
        $data['categories'] = Category::all();
        return view('product.create',$data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|alpha',
            'brand_id' => 'required|exists:brands,id',
            'categorie_id' => 'required|exists:categories,id'
            ]);
        $product = new Product();
        $product['name'] = $request->get('name');
        $product['brand_id'] = $request->get('brand_id');
        $product['categorie_id'] = $request->get('categorie_id');
        $product->save();
        return redirect()->route('product.create')->with('success','Product Inserted Successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['products'] = Product::find($id);
        $data['brands'] = Brand::all();
        $data['categories'] = Category::all();
        return view('product.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|alpha',
        ]);
        $product = Product::findOrFail($id);
        $product['name'] = $request->get('name');
        $product['brand_id'] = $request->get('brand_id');
        $product['categorie_id'] = $request->get('categorie_id');
        $product->save();
        return redirect()->route('product.create')->with('success','Product Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('product.create')->with('success','Product Deleted');
    }
}
