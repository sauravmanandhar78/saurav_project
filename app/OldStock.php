<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldStock extends Model
{


    protected $fillable=['stock_id',	'product_name',	'categories_id'	,'brand_id'	,'image'	,'product_quantity'	,'product_price'];
}
