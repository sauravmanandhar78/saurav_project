<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);


        Gate::define('isAdmin',function ($user){
           return $user->user_type == 'admin';
        });
        Gate::define('isStaff',function($user){
           return $user->user_type == 'staff';
        });
        Gate::define('isManager',function ($user){
            return $user->user_type == 'manager';
        });


        //
    }
}
