<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Saurav',
            'last_name' => 'Manandhar',
            'address' => 'naxal',
            'gender' => 'male',
            'email' => 'admin@gmail.com',
            'phone' => '9841661481',
            'image' => '56eAOduzYqqitJfQ.jpg',
            'user_type' => 'admin',
            'password' => Hash::make('password')
        ]);
    }
}
